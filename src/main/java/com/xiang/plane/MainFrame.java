package com.xiang.plane;


import com.xiang.plane.before.Before;
import com.xiang.plane.fight.GameHandler;
import com.xiang.plane.gamevoer.GameOver;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Create by Administrator on 2017/8/2
 *
 * @DESCRIPTION
 */
public class MainFrame extends JFrame {
    public static int FPS = 100;

    //窗体的宽和高
    public static final int WIDTH = 480;
    public static final int HEIGHT = 800;

    private Image imageBuffer;
    private Before before;
    private GameHandler gameHandler;
    private GameOver gameOver;

    public MainFrame() {
        init();
        before = new Before();
        gameHandler = new GameHandler();
        gameOver = new GameOver();
    }

    private void init() {
        this.setTitle("aaa");
        this.setSize(WIDTH, HEIGHT);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setVisible(true);
        this.setAlwaysOnTop(true);
        //使用定时器刷新界面
        final Timer timer = new Timer();
        //40ms时开始执行，之后每40ms执行一次
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (MainApp.status == 1) {
                    //处理游戏逻辑
                    gameHandler.logical();
                }
                //绘制到窗体上
                repaint();
            }
        }, 40, 1000/FPS);
        //在窗口关闭时取消定时任务
        this.addWindowStateListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                timer.cancel();
            }
        });
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                //在不同的界面互不影响
               if (MainApp.status == 0) {
                   before.keyPressed(e);
               } else if (MainApp.status == 1) {
                   gameHandler.keyPressed(e);
               } else if (MainApp.status == 2) {
                   gameOver.keyPressed(e);
               }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (MainApp.status == 0) {
                    before.keyReleased(e);
                } else if (MainApp.status == 1) {
                    gameHandler.keyReleased(e);
                } else if (MainApp.status == 2) {
                    gameOver.keyReleased(e);
                }
            }
        });
    }

    @Override
    public void paint(Graphics g) {
        //创建一张缓冲图片
        if (imageBuffer == null) {
            imageBuffer = this.createImage(WIDTH, HEIGHT);
        }
        //得到缓冲图片的画笔
        Graphics g4Image = imageBuffer.getGraphics();
        draw(g4Image);
        //将图片画到窗体上
        g.drawImage(imageBuffer, 0, 0, null);
        g4Image.dispose();
        g.dispose();
    }

    private void draw(Graphics g) {
        if (MainApp.status == 0) {
            before.draw(g);
        } else if (MainApp.status == 1) {
            gameHandler.draw(g);
        } else if (MainApp.status == 2) {
            gameOver.draw(g);
        }
    }
}
