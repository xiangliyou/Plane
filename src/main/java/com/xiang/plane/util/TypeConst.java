package com.xiang.plane.util;

public interface TypeConst {
	//我方普通子弹
	int MY_BULLET_NORMAL_1 =1;
	//我方直线向量子弹
	int MY_BULLET_LINE =3;
	//我方向量跟踪子弹
	int MY_BULLET_VECTOR_TRACK=5;

	//发射一颗子弹的射击方式
    int MY_SHOT_SINGLE_BULLET_1 =1;
	//发射多颗子弹的射击方式
	int MY_SHOT_MULITI_BULLET_1 =2;
	//发射多颗向量追踪子弹
	int MY_SHOT_VECTOR_TRACK_1=3;
	//发射多颗向量追踪子弹2
	int MY_SHOT_VECTOR_TRACK_2 = 4;


    //左贝塞尔子弹
    int MY_BULLET_BZ_TRACK_1 = 6;
    //右贝塞尔子弹
    int MY_BULLET_BZ_TRACK_2 = 7;
    //发射贝塞尔子弹
    int MY_SHOT_BZ_BULLET_1 = 8;

	//敌方的普通子弹
	int ENEMY_BULLET_NORMAL_1 =2;
	//敌方跟踪子弹
	int ENEMY_BULLET_TRACK_1 =4;


	//***************敌机的运动方式*******************/
    //正常
    int ENEMY_MOVE_TYPE_NORMAL_1 = 10;
    //跟踪
	int ENEMY_MOVE_TYPE_TRACK_1 = 11;
    //BZ曲线
	int ENEMY_MOVE_TYPE_BZ_1 = 12;
}
