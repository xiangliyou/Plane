package com.xiang.plane.util;
public class PVector {
	public float x, y;

	public PVector(double x1, double x2) {
		this.x = (float) x1;
		this.y = (float) x2;
	}
	//向量相加
	public PVector add(PVector v) {
		float x = this.x + v.x;
		float y = this.y + v.y;
		return new PVector(x, y);
	}

	//向量相减
	public PVector sub(PVector v) {
		float x = this.x - v.x;
		float y = this.y - v.y;
		return new PVector(x, y);
	}

	//返回一个新的PVector的副本
	public PVector newPVector(){
		return new PVector(this.x,this.y);
	}

	//将向量按逆时针绕原点旋转
	public void rotateCW(double radian) {
		double rx = (this.x* Math.cos(radian))+ (this.y* Math.sin(radian));
		double ry =(this.y* Math.cos(radian))- (this.x* Math.sin(radian));
		x= (float) rx;
		y= (float) ry;
		this.x=x;
		this.y=y;
	}
	//将向量旋转任意角度
	public void rotateAngle(int angle){
		if (angle >0){
			this.rotateCWAngle(angle);
		}else{
			angle =-angle;
			this.rotateCCWAngle(angle);
		}
	}

	//将向量逆时针绕原点旋转
	public void rotateCWAngle(double angle) {
		double radian = PVector.transAngle(angle);
		rotateCW(radian);
	}

	// 将向量逆时针绕原点旋转
	public void rotateCCW(double radian) {
		double rx = (this.x* Math.cos(radian))- (this.y* Math.sin(radian));
		double ry = (this.x* Math.sin(radian))+ (this.y* Math.cos(radian));
		this.x= (float) rx;
		this.y= (float) ry;
	}

	//将向量逆时针绕原点旋转
	public void rotateCCWAngle(double angle) {
		double radian = PVector.transAngle(angle);
		rotateCCW(radian);
	}
	//角度换成弧度
	public static double transAngle(double angle){
		return Math.PI/180*angle;
	}

	// 检测两个向量之间的夹角(返回角度)
	public double checkVectorAngle(PVector p2){
		double n = this.x*p2.x+this.y*p2.y;
		double m =Math.sqrt(this.x*this.x+this.y*this.y)*Math.sqrt(p2.x*p2.x+p2.y*p2.y);
		return Math.acos(n/m);
	}

	//取模运算
	public float getLength (){
		float dis = Calculate.getDistance(this.x, this.y).floatValue();
		return dis;
	}

	// 归一化
	public PVector normalize() {
		float dis =this.getLength();
		if (dis == 0)
			dis = 0.00000000001f;
		float x1 = this.x / dis;
		float x2 = this.y / dis;
		return new PVector(x1, x2);
	}

	//设置向量的x,y分量
	public void setVector(float x, float y){
		this.x=x;
		this.y=y;
	}
}
