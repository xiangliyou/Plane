package com.xiang.plane.util;

public class Calculate {
	/**
	 *  判断两个矩形是否相交
	 *  参数为两个矩形的左上角坐标和其长宽
	 * @return 返回true表示碰撞成功了
	 */
	public static boolean isRectCollision(int x1,int y1,int width1,int height1,
										  int x2,int y2,int width2,int height2){
		return x2 + width2 > x1 && x2 < x1 + width1
				&& y2 + height2 > y1 && y2 < y1 + height1;
	}

	// 计算两点间的距离
	public static Double getDistance(double x1, double y1, double x2, double y2) {
		double x = x2 - x1;
		double y = y2 - y1;
		return Math.hypot(x, y);

	}

	// 计算两点间的距离
	public static Double getDistance(double x1, double y1) {
		return getDistance(0, 0, x1, y1);
	}

}
