package com.xiang.plane.util;

import com.xiang.plane.MainFrame;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by xiangliyou on 17-8-5.
 *
 * @DESCRIPTION
 *      贝塞尔曲线
 */
public class BZCarve {
    //起始点
    private Point start = new Point(0, 0);
    //控制点
    private Point inflect = new Point(0, 0);
    //目标点
    private Point target = new Point(0, 0);
    //路径点列表
    private List<Point> pointList = new ArrayList<>();

    /**
     * @param start  起始点    飞机坐标
     * @param inflect   控制点 始终的中点
     * @param target    目标点 敌机
     */
    public BZCarve(Point start, Point inflect, Point target) {
        this.start = start;
        this.inflect = inflect;
        this.target = target;
    }

    //产生贝塞尔坐标
    private Point checkBZ(float t) {
        //贝塞尔曲线公式 (1 - t)^2 P0 + 2 t (1 - t) P1 + t^2 P2（t在0-1范围内）
        float rx = (1 - t) * (1 - t) * start.x + 2 * t * (1 - t) * inflect.x + t * t * target.x;
        float ry = (1 - t) * (1 - t) * start.y + 2 * t * (1 - t) * inflect.y + t * t * target.y;
        return new Point((int) rx, (int) ry);
    }

    //初始化，并计算所有贝塞尔路径点
    public void init() {
        float r = 0;
        //生成路径点,50个,每个t增加0.08
        for (int i = 0; i < MainFrame.FPS; i++) {
            int t = i;
            r = r + 0.08f;
            //计算出路径点并加入到列表中
            pointList.add(this.checkBZ(r));
        }
    }

    public List<Point> getPointList() {
        return pointList;
    }
}
