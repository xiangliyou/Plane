package com.xiang.plane.util;

import com.xiang.plane.MainApp;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Create by Xiang Liyou on 2017/8/4
 *
 * @DESCRIPTION
 */
public class ImageUtil {

    /**
     * 根据图片路径加载图片
     * @param path 图片路径
     * @return 加载好的图片
     */
    public static BufferedImage loadImage(String path) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(MainApp.class.getClassLoader().getResourceAsStream(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }



    /**
     * 用到的图片资源
     *      KEY ： 图片的KEY
     *      VALUE：对应的图片
     */
    public final static Map<Integer, BufferedImage> IMAGE_MAP = new HashMap<>();

    //英雄图片的KEY
    public final static int HERO_1_KEY = 1001;
    //背景图片的KEY
    public final static int BACK_1_KEY = 2001;
    //敌机图片的KEY
    public final static int ENEMY_1_KEY = 3001;
    public final static int ENEMY_2_KEY = 3002;

    //敌机子弹
    public final static int ENEMY_BULLET_1_KEY = 4001;
    //英雄子弹
    public final static int HERO_BULLET_1_KEY = 5001;

    static {
        BufferedImage tmp;
        //加载英雄飞机
        tmp = ImageUtil.loadImage("img/shoot/hero1.png");
        IMAGE_MAP.put(HERO_1_KEY, tmp);

        //加载敌方飞机
        tmp = ImageUtil.loadImage("img/shoot/enemy1.png");
        IMAGE_MAP.put(ENEMY_1_KEY, tmp);
        tmp = ImageUtil.loadImage("img/shoot/enemy2.png");
        IMAGE_MAP.put(ENEMY_2_KEY, tmp);

        //加载子弹
        tmp = ImageUtil.loadImage("img/shoot/bullet1.png");
        IMAGE_MAP.put(HERO_BULLET_1_KEY, tmp);
        tmp = ImageUtil.loadImage("img/shoot/bullet2.png");
        IMAGE_MAP.put(ENEMY_BULLET_1_KEY, tmp);

        //加载背景图片
        tmp = ImageUtil.loadImage("img/shoot_background/background.png");
        IMAGE_MAP.put(BACK_1_KEY, tmp);
    }


}
