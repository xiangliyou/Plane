package com.xiang.plane.fight;

import com.xiang.plane.MainApp;
import com.xiang.plane.MainFrame;
import com.xiang.plane.util.Timer;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Create by Xiang Liyou on 2017/8/3
 *
 * @DESCRIPTION
 *      进行游戏的逻辑处理
 */
public class GameHandler {
    //背景
    private Back back;
    //我方飞机
    public static Hero hero;
    //敌方飞机的集合
    public static volatile  List<Enemy> enemyList = new ArrayList<Enemy>();
    //子弹集合
    public static volatile List<Bullet> bulletList = new ArrayList<Bullet>();

    //产生敌机的计时器
    Timer enemyTimer = new Timer(MainFrame.FPS);

    public GameHandler() {
        back = new Back();
        hero = new Hero();
    }

    public void draw(Graphics g) {
        back.draw(g);
        //绘画敌机
        for (int i = 0; i < enemyList.size(); i++) {
            enemyList.get(i).draw(g);
        }
        //绘画子弹
        for (int i = 0; i < bulletList.size(); i++) {
            Bullet bullet =  bulletList.get(i);
            if (bullet != null) {
                bullet.draw(g);
            }
        }
        hero.draw(g);
    }

    /**
     * 每一帧的逻辑
     */
    public void logical() {
        //加入敌机
        if (enemyTimer.act()) {
            new Enemy();
        }

        //更新敌机
        for (int i = 0; i < enemyList.size(); i++) {
            enemyList.get(i).update();
        }
        //更新子弹
        for (int i = 0; i < bulletList.size(); i++) {
            Bullet bullet =  bulletList.get(i);
            if (bullet != null) {
                bullet.update();
            }
        }

    }


    public void keyPressed(KeyEvent e) {
        hero.keyPressed(e);
    }

    public void keyReleased(KeyEvent e) {
        hero.keyReleased(e);
    }
}
