package com.xiang.plane.fight;

import com.xiang.plane.MainFrame;
import com.xiang.plane.util.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Random;


public class Enemy {
    //敌机的图片
    private BufferedImage enemy;
    //敌机的坐标
    public int x, y;
    //敌机的宽高
    public int width, height;

    //速度
    private int speed = 2;
    //生命值
    public int life = 50;
    //是否爆炸状态，是否爆炸结束状态
    public boolean isExplode = false, isExplodeDead = false;
    //爆炸半径,最大爆炸半径
    private int explodeRadius = 0, explodeMaxRadius = 100;
    //这里设置一秒钟产生1颗子弹
    private Timer fireTimer = new Timer(MainFrame.FPS);
    //贝塞尔曲线的路径点列表
    private List<Point> pointList = null;
    //当前应该取得的路径点索引
    private int pathIndex = 0;
    //敌机的类型
    private int bulletType;
    //目标向量
    private PVector targetV = new PVector(MainFrame.WIDTH, MainFrame.HEIGHT);
    //默认时垂直向上的
    private PVector initV = new PVector(0, 0);

    private int moveType = TypeConst.ENEMY_MOVE_TYPE_NORMAL_1;
    public Enemy() {
        //创建一个随机数生成器
        Random r = new Random();
        long tmp = Math.round(Math.random() * (ImageUtil.ENEMY_2_KEY - ImageUtil.ENEMY_1_KEY) + ImageUtil.ENEMY_1_KEY);
        int enemyType = (int) tmp;
        enemy = ImageUtil.IMAGE_MAP.get(enemyType);
        this.width = enemy.getWidth();
        this.height = enemy.getHeight();
        //随机产生一个从0到窗口宽度减敌机宽度之间的整数，不让敌机从最右边看不到的地方出来
        this.x = r.nextInt(MainFrame.WIDTH-this.width);
        //this.x = 0;
        this.y = -10;
        GameHandler.enemyList.add(this);


        //设置贝塞尔曲线控制点
        int inflectX = (int) (x + (this.targetV.x-x)/2)-MainFrame.WIDTH/2;
        int inflectY = (int) (y + (this.targetV.y-y)/2)-MainFrame.HEIGHT/2;
        //创建贝塞尔曲线  传入贝塞尔曲线起点 ,传入贝塞尔曲线控制点,传入贝塞尔曲线终点
        BZCarve crave=new BZCarve(new Point(x, y),new Point(inflectX,inflectY),
                new Point((int)this.targetV.x,(int)this.targetV.y));
        //初始化贝塞尔曲线的计算，获得路径点
        crave.init();
        //获取路径点列表
        this.pointList = crave.getPointList();
    }

    public void draw(Graphics g) {
        g.setColor(Color.RED);
        //如果爆炸状态开启，就开始画爆炸的效果
        if (this.isExplode) {
            //画爆炸效果
           drawExplodeType(g);
        }

        //画敌机
        g.drawImage(enemy, x, y, null);
    }

    //根据敌机类型画不同的爆炸效果
    private void drawExplodeType(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.RED);
        //设置线宽
        g2d.setStroke(new BasicStroke(4f));
        //画一个圆圈表示爆炸效果
        g.drawOval(x - 40, y - 40, explodeRadius, explodeRadius);
        //每一帧让圆的半径增大15
        this.explodeRadius += 20;
        if (this.explodeRadius > this.explodeMaxRadius) {
            this.isExplodeDead = true;
        }
    }

    public void update() {
        //每帧追踪敌机
        this.move();
        this.checkDead();
        this.fire();
    }

    //敌机的移动
    private void move() {
        switch (moveType) {
        case TypeConst.ENEMY_BULLET_TRACK_1://自杀式追踪敌机
            int mx = GameHandler.hero.getHeroX();
            //这里就是简单追踪
            if (this.x < mx) {
                this.x += speed;
            } else {
                this.x -= speed;
            }
            this.y = this.y + speed;
            break;
        case TypeConst.ENEMY_MOVE_TYPE_BZ_1:
            Point targetPosR = this.pointList.get(pathIndex++);
            this.x = targetPosR.x;
            this.y = targetPosR.y;
            break;
        case TypeConst.ENEMY_MOVE_TYPE_NORMAL_1:
            this.y = this.y + speed;
            break;
        }


    }

    public void fire() {
        //产生一颗子弹，位置就在敌人飞机的正前方
        if (this.fireTimer.act()) {
            //这里的+20和+55用来调整子弹的初始位置，让它从飞机的正前方打出来
            Bullet temp = new Bullet(this.x + enemy.getWidth() / 2, this.y + enemy.getHeight(),
                    true, TypeConst.ENEMY_BULLET_TRACK_1);
            //改变它的方向向量
            temp.setTargetV(new PVector(GameHandler.hero.getHeroX() - temp.x + GameHandler.hero.getHeroWidthHalf(),
                    GameHandler.hero.getHeroY() - temp.y + GameHandler.hero.getHeroHeight()).normalize());
        }
    }

    //检测是不是应该被销毁
    private void checkDead() {
        boolean outHeight = this.y > MainFrame.HEIGHT;
        //如果高度越界就销毁
        if (outHeight) this.destroy();
        //如果爆炸效果结束了，那么也销毁
        if (this.isExplodeDead) this.destroy();
    }

    //受到伤害，掉血
    public void beHit() {
        //如果没有血了，就开始爆炸动画
        if (--this.life < 0) this.isExplode = true;
    }

    //销毁自己，释放资源
    private synchronized void destroy() {
        GameHandler.enemyList.remove(this);
        //让图像到窗口外面去
        this.y = 1000;
        this.x = 1000;
    }

}
