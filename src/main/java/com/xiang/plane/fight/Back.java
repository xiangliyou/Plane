package com.xiang.plane.fight;

import com.xiang.plane.MainFrame;
import com.xiang.plane.util.FrameRate;
import com.xiang.plane.util.ImageUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Create by Xiang Liyou on 2017/8/3
 *
 * @DESCRIPTION
 */
public class Back {
    //背景图片
    private BufferedImage back;
    //两张背景拼接的y坐标
    private int y1 = 0;
    private int y2 = 0;
    //fps计算
    private FrameRate frameRate;
    //背景移动的速度
    public int backSpeed = 5;

    public Back() {
        y1 = 0;
        y2 = -MainFrame.HEIGHT;
        this.back = ImageUtil.IMAGE_MAP.get(ImageUtil.BACK_1_KEY);
        frameRate = new FrameRate();
        frameRate.initialize();
    }

    public void draw(Graphics g) {
        g.fillRect(0, 0, MainFrame.WIDTH, MainFrame.HEIGHT);
        g.drawImage(back, 0, y1, null);
        g.drawImage(back, 0, y2, null);
        g.setColor(new Color(5,100,222));
        g.setFont(new Font("楷体", Font.BOLD, 18));
       //g.drawString(Const.score+"", 5,50);

        //计算fps并绘画到窗体上
        frameRate.calculate();
        g.setColor(Color.RED);
        g.drawString(frameRate.getFrameRate(), 5, 70);

        move();
    }

    private void move() {
        y1 += backSpeed;
        y2 += backSpeed;

        if (y1 >= MainFrame.HEIGHT) {
            y1 = -MainFrame.HEIGHT;
        }
        if (y2 >= MainFrame.HEIGHT) {
            y2 = -MainFrame.HEIGHT;
        }
    }

}
