package com.xiang.plane.fight;

import com.xiang.plane.MainFrame;
import com.xiang.plane.util.ImageUtil;
import com.xiang.plane.util.PVector;
import com.xiang.plane.util.Timer;
import com.xiang.plane.util.TypeConst;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

/**
 * Create by Xiang Liyou on 2017/8/3
 *
 * @DESCRIPTION 我方飞机
 */
public class Hero {
    private BufferedImage hero;
    private int heroX;
    private int heroY;

    private int heroWidthHalf;
    private int heroHeight;

    private boolean isUp;
    private boolean isDown;
    private boolean isLeft;
    private boolean isRight;

    private boolean isFire = true;
    //产生我军子弹的定时器
    Timer bulletTimer = new Timer(1);
    //hero的速度
    public int heroSpeed = 10;
    //发射的子弹类型
    private int bulletType = TypeConst.MY_SHOT_BZ_BULLET_1;

    public Hero() {
        this.hero = ImageUtil.IMAGE_MAP.get(ImageUtil.HERO_1_KEY);
        heroWidthHalf = hero.getWidth() / 2;
        heroHeight = hero.getHeight();
        heroX = MainFrame.WIDTH / 2 - heroWidthHalf;
        heroY = MainFrame.HEIGHT - heroHeight;
    }

    public void draw(Graphics g) {
        //绘画英雄飞机
        g.drawImage(hero, heroX, heroY, null);
        move();
        //开火
        if (isFire && bulletTimer.act()) {
            fire();
        }
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            isUp = true;
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            isDown = true;
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            isLeft = true;
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            isRight = true;
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            isFire = true;
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            isUp = false;
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            isDown = false;
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            isLeft = false;
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            isRight = false;
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            isFire = false;
        }
    }

    private void move() {
        //开始移动
        if (isUp) {
            heroY -= heroSpeed;
        }
        if (isDown) {
            heroY += heroSpeed;
        }
        if (isLeft) {
            heroX -= heroSpeed;
        }
        if (isRight) {
            heroX += heroSpeed;
        }
        //判断是否到边界
        if (heroX <= -heroWidthHalf + 10) {
            heroX = -heroWidthHalf + 10;
        }
        if (heroX >= MainFrame.WIDTH - heroWidthHalf - 10) {
            heroX = MainFrame.WIDTH - heroWidthHalf - 10;
        }
        if (heroY <= 50) {
            heroY = 50;
        }
        if (heroY >= MainFrame.HEIGHT - heroHeight) {
            heroY = MainFrame.HEIGHT - heroHeight;
        }
    }


    private void fire() {
        switch (this.bulletType) {
            //普通子弹
            case TypeConst.MY_SHOT_SINGLE_BULLET_1:
                //发射我军子弹
                new Bullet(heroX + heroWidthHalf, heroY - 5, false, bulletType);
                break;
            //散弹
            case TypeConst.MY_SHOT_MULITI_BULLET_1:
                makeMulitiBullet(5, 5);
                break;
            //跟踪弹1号
            case TypeConst.MY_SHOT_VECTOR_TRACK_1:
                for (int i = 1; i <= 5; i++) {
                    new Bullet(heroX - i * 5 + heroWidthHalf, heroY - 5, false, TypeConst.MY_BULLET_VECTOR_TRACK).setInitV(new PVector(0, -1));
                }
                new Bullet(heroX + heroWidthHalf, heroY - 5, false, TypeConst.MY_BULLET_VECTOR_TRACK).setInitV(new PVector(0, -1));
                for (int i = 1; i <= 5; i++) {
                    new Bullet(heroX + i * 5 + heroWidthHalf, heroY - 5, false, TypeConst.MY_BULLET_VECTOR_TRACK).setInitV(new PVector(0, -1));
                }
                break;
            //跟踪弹2号
            case TypeConst.MY_SHOT_VECTOR_TRACK_2:
                new Bullet(heroX - 10 + heroWidthHalf, heroY - 5, false, TypeConst.MY_BULLET_VECTOR_TRACK).setInitV(new PVector(-5, 1));
                new Bullet(heroX + heroWidthHalf, heroY - 5, false, TypeConst.MY_BULLET_VECTOR_TRACK).setInitV(new PVector(0, -1));
                new Bullet(heroX + 10 + heroWidthHalf, heroY - 5, false, TypeConst.MY_BULLET_VECTOR_TRACK).setInitV(new PVector(5, 1));
                break;
            //贝塞尔子弹
            case TypeConst.MY_SHOT_BZ_BULLET_1:
                new Bullet(heroX - 20 + heroWidthHalf, heroY - 5, false, TypeConst.MY_BULLET_BZ_TRACK_1).setInitV(new PVector(0, 0));
                new Bullet(heroX + 20 + heroWidthHalf, heroY - 5, false, TypeConst.MY_BULLET_BZ_TRACK_2).setInitV(new PVector(0, 0));
                break;
        }

    }

    /**
     * 产生多颗子弹
     *
     * @param bulletNum  子弹数量
     * @param amongAngle 子弹角度的间隔
     */
    private void makeMulitiBullet(int bulletNum, int amongAngle) {
        //先建一颗垂直向上的子弹
        new Bullet(this.heroX + heroWidthHalf, this.heroY - 10, false, TypeConst.MY_BULLET_LINE).setInitV(new PVector(0, -1));
        //判断左右偏转
        boolean flag = false;
        //用来计算偏移角度
        int num = 1;
        while (bulletNum-- > 0) {
            int rotateNum = num / 2;
            //设置初始向量
            PVector initV = new PVector(0, -1);
            //如果flag是true就将向量向左边偏转，否则向右偏转
            if (flag)
                initV.rotateAngle(amongAngle * rotateNum);
            else
                initV.rotateAngle(-amongAngle * rotateNum);
            //新建一个旋转的直线向量子弹
            new Bullet(this.heroX + heroWidthHalf, this.heroY - 10, false, TypeConst.MY_BULLET_LINE).setInitV(initV);
            //转换标记
            flag = !flag;
            num++;
        }
    }

    public int getHeroX() {
        return heroX;
    }

    public int getHeroY() {
        return heroY;
    }

    public int getHeroWidthHalf() {
        return heroWidthHalf;
    }

    public int getHeroHeight() {
        return heroHeight;
    }
}
