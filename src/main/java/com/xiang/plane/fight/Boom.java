package com.xiang.plane.fight;

import com.xiang.plane.util.ImageUtil;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Create by Xiang Liyou on 2017/8/6
 *
 * @DESCRIPTION
 */
public class Boom {
    BufferedImage boom1;
    BufferedImage boom2;
    BufferedImage boom3;
    BufferedImage boom4;

    public Boom() {
        boom1 = ImageUtil.loadImage("img/shoot/enemy1_down1.png");
        boom2 = ImageUtil.loadImage("img/shoot/enemy1_down2.png");
        boom3 = ImageUtil.loadImage("img/shoot/enemy1_down3.png");
        boom4 = ImageUtil.loadImage("img/shoot/enemy1_down4.png");
    }


    public void draw(Graphics g) {

    }
}
