package com.xiang.plane.fight;

import com.xiang.plane.MainFrame;
import com.xiang.plane.util.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
/**
 * Create by Xiang Liyou on 2017/8/4
 *
 * @DESCRIPTION 子弹类
 */
public class Bullet {

    public int x, y;
    public int width, height;
    //子弹速度
    private int speed = 10;
    //飞机的图片
    private BufferedImage bullet;
    //是否是敌机
    private boolean isEnemy = false;
    //子弹类型
    private int bulletType = TypeConst.MY_BULLET_NORMAL_1;
    //是否爆炸状态，是否爆炸结束状态
    public boolean isExplode = false, isExplodeDead = false;
    //爆炸半径,最大爆炸半径
    private int explodeRadius = 0, explodeMaxRadius = 30;
    //目标向量
    private PVector targetV = new PVector(0, 0);
    //默认时垂直向上的
    private PVector initV = new PVector(-1, -1);
    //旋转角度
    private double rotateAngle;
    //贝塞尔曲线的路径点列表
    private List<Point> pointList = null;
    //当前应该取得的路径点索引
    private int pathIndex = 0;

    public Bullet(int x, int y) {
        this.x = x;
        this.y = y;
        //加入子弹集合
        GameHandler.bulletList.add(this);
    }

    /**
     * @param x       子弹x坐标
     * @param y       子弹y坐标
     * @param isEnemy true为是敌机子弹
     */
    public Bullet(int x, int y, boolean isEnemy) {
        this(x, y);
        this.isEnemy = isEnemy;
    }

    /**
     * @param x          子弹x坐标
     * @param y          子弹y坐标
     * @param isEnemy    true为是敌机子弹
     * @param bulletType 子弹类型，引用com.xiang.plane.util.TypeConst里的值
     */
    public Bullet(int x, int y, boolean isEnemy, int bulletType) {
        this(x, y, isEnemy);
        this.bulletType = bulletType;
        if (isEnemy) {
            bullet = ImageUtil.IMAGE_MAP.get(ImageUtil.ENEMY_BULLET_1_KEY);
        } else {
            bullet = ImageUtil.IMAGE_MAP.get(ImageUtil.HERO_BULLET_1_KEY);
        }

        this.width = bullet.getWidth();
        this.height = bullet.getHeight();
        //设置左向贝塞尔曲线
        if (bulletType == TypeConst.MY_BULLET_BZ_TRACK_1){
            this.targetV=this.getNestEnemy();
            //设置贝塞尔曲线控制点
            int inflectX = (int) (x + (this.targetV.x-x)/2)-MainFrame.WIDTH/2;
            int inflectY = (int) (y + (this.targetV.y-y)/2)-MainFrame.HEIGHT/2;
            //创建贝塞尔曲线  传入贝塞尔曲线起点 ,传入贝塞尔曲线控制点,传入贝塞尔曲线终点
            BZCarve crave=new BZCarve(new Point(x, y),new Point(inflectX,inflectY),
                    new Point((int)this.targetV.x,(int)this.targetV.y));
            //初始化贝塞尔曲线的计算，获得路径点
            crave.init();
            //获取路径点列表
            this.pointList = crave.getPointList();
        }
        //设置右向贝塞尔曲线（同上）
        if (this.bulletType == TypeConst.MY_BULLET_BZ_TRACK_2 ){
            this.targetV=this.getNestEnemy();
            //设置贝塞尔曲线控制点
            int inflectX = (int) (x + (this.targetV.x - x) / 2) + MainFrame.WIDTH/2;
            int inflectY = (int) (y + (this.targetV.y - y) / 2) - MainFrame.HEIGHT/2;
            //创建贝塞尔曲线  传入贝塞尔曲线起点 ,传入贝塞尔曲线控制点,传入贝塞尔曲线终点
            BZCarve crave=new BZCarve(new Point(x, y),new Point(inflectX,inflectY),
                    new Point((int)this.targetV.x,(int)this.targetV.y));
            //初始化贝塞尔曲线的计算，获得路径点
            crave.init();
            //获取路径点列表
            this.pointList = crave.getPointList();
        }
    }

    public void draw(Graphics g) {
        if (this.isExplode) {
            drawExplodeType(g);
        } else {
            this.drawBulletType(g);
        }
    }

    //可以根据子弹类型画不同的击中效果
    private void drawExplodeType(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.ORANGE);
        //设置线宽
        g2d.setStroke(new BasicStroke(2f));
        //画一个圆圈表示爆炸效果
        g.drawOval(x - 10, y - 10, explodeRadius, explodeRadius);
        //每一帧让圆的半径增大15
        this.explodeRadius += 10;
        if (this.explodeRadius > this.explodeMaxRadius) {
            this.isExplodeDead = true;
        }
    }


    //根据子弹的类型来渲染内容
    private void drawBulletType(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        switch (bulletType) {
            case TypeConst.MY_BULLET_NORMAL_1://我方普通子弹
                g.drawImage(bullet, x, y, null);
                break;
            case TypeConst.MY_BULLET_LINE://我方斜线子弹
                //旋转计算出的角度
                g2d.rotate(this.rotateAngle, this.x, this.y);
                //画
                g2d.drawImage(bullet, x, y, null);
                //还原角度
                g2d.rotate(-this.rotateAngle, this.x, this.y);
                break;
            case TypeConst.MY_BULLET_VECTOR_TRACK://跟踪弹
                //旋转计算出的角度
                g2d.rotate(this.rotateAngle, this.x, this.y);
                //画
                g2d.drawImage(bullet, x, y, null);
                //还原角度
                g2d.rotate(-this.rotateAngle, this.x, this.y);
                break;
            case TypeConst.MY_BULLET_BZ_TRACK_1://左贝塞尔
                //旋转计算出的角度
                g2d.rotate(this.rotateAngle, this.x, this.y);
                //画
                g2d.drawImage(bullet, x, y, null);
                //还原角度
                g2d.rotate(-this.rotateAngle, this.x, this.y);
                break;
            case TypeConst.MY_BULLET_BZ_TRACK_2://右贝塞尔
                //旋转计算出的角度
                g2d.rotate(this.rotateAngle, this.x, this.y);
                //画
                g2d.drawImage(bullet, x, y, null);
                //还原角度
                g2d.rotate(-this.rotateAngle, this.x, this.y);
                break;

            case TypeConst.ENEMY_BULLET_NORMAL_1://敌方普通子弹
                g.drawImage(bullet, x, y, null);
                break;
            case TypeConst.ENEMY_BULLET_TRACK_1://敌方跟踪弹
                //旋转计算出的角度
                g2d.rotate(this.rotateAngle, this.x, this.y);
                //画
                g2d.drawImage(bullet, x, y, null);
                //还原角度
                g2d.rotate(-this.rotateAngle, this.x, this.y);
                break;
        }
    }


    //根据子弹的类型更新内容
    private void updateBulletType() {

        switch (this.bulletType) {
            case TypeConst.MY_BULLET_NORMAL_1://我方普通子弹
                //设置初始方向向量
                initV.setVector(0, 0);
                this.y -= speed;
                break;
            case TypeConst.MY_BULLET_LINE:
                this.setSpeed(15);
                break;
            case TypeConst.MY_BULLET_VECTOR_TRACK://跟踪弹
                this.setSpeed(10);
                targetV = getNestEnemy();
                //取得当前的目标向量
                PVector tp = new PVector((targetV.x - this.x), (targetV.y - this.y));
                //让初始向量的模向量加上目标向量的模向量
                initV = initV.normalize().add(tp.normalize());
                break;
            case TypeConst.MY_BULLET_BZ_TRACK_1://左贝塞尔
                Point targetPosL = this.pointList.get(pathIndex++);
                this.x =targetPosL.x;
                this.y =targetPosL.y;
                break;
            case TypeConst.MY_BULLET_BZ_TRACK_2://右贝塞尔
                Point targetPosR = this.pointList.get(pathIndex++);
                this.x =targetPosR.x;
                this.y =targetPosR.y;
                break;

            case TypeConst.ENEMY_BULLET_NORMAL_1://敌方普通子弹
                initV.setVector(0, 0);
                this.y += speed / 2;
                break;
            case TypeConst.ENEMY_BULLET_TRACK_1:
                this.setSpeed(3);
                initV = targetV;
                break;
        }
    }

    //得到离自己最近的敌机的坐标，并返回为向量
    public PVector getNestEnemy() {
        int result = 0;
        int index = -1;
        //循环遍历敌人类，寻找y轴坐标最大的飞机
        for (int i = 0; i < GameHandler.enemyList.size(); i++) {
            int yPos = GameHandler.enemyList.get(i).y;
            if (yPos > result) {
                result = yPos;
                index = i;
            }
        }
        //如果屏幕上没有飞机，就让子弹垂直往上射
        if (index == -1) return new PVector(this.x + GameHandler.hero.getHeroWidthHalf(), -1);
        //如果有最近的敌机，返回最近敌机的中间偏下的坐标
        Enemy nestEnemy = GameHandler.enemyList.get(index);
        return new PVector(nestEnemy.x + nestEnemy.width / 2, nestEnemy.y + nestEnemy.height);
    }

    private void checkDead() {
        boolean outHeight = this.y > MainFrame.HEIGHT || this.y < 0;
        boolean outWeight = this.x > MainFrame.WIDTH || this.x < 0;
        //在窗体外面
        if (outHeight || outWeight || isExplodeDead) {
            this.destroy();
        }
    }

    //销毁自己，释放资源
    private synchronized void destroy() {
        //从列表中移除，使在程序中没有此对象的引用
        GameHandler.bulletList.remove(this);
        //移除到窗体外面
        this.y = 1000;
        this.x = 1000;
    }

    /**
     * 更新子弹的状态
     */
    public void update() {
        this.checkDead();
        this.updateBulletType();
        this.checkEnemyCollision();
        if (this.bulletType == TypeConst.MY_BULLET_BZ_TRACK_1 ||
                this.bulletType == TypeConst.MY_BULLET_BZ_TRACK_2) return;

        //将x,y坐标，分别加上初始方向模向量的x,y分量*速度值
        this.x = (int) (this.x + initV.x * speed);
        this.y = (int) (this.y + initV.y * speed);

        //计算子弹旋转的角度
        //区分是左偏于垂直线还是右偏于垂直线
        if (initV.x < 0) {//左
            //求initV向量与垂直向量之间的夹角
            this.rotateAngle = -new PVector(0, -1).checkVectorAngle(initV);
        } else {//右
            this.rotateAngle = new PVector(0, -1).checkVectorAngle(initV);
        }
    }

    //遍历检测与敌机的碰撞
    private void checkEnemyCollision() {
        //如果这颗子弹是敌机打出来的，那么不进行与敌机的碰撞检测
        if (this.isEnemy) {
            return;
        }
        //检测这颗子弹与每一辆敌机是否碰撞
        for (int i = 0; i < GameHandler.enemyList.size(); i++) {
            boolean result = this.judgeEnemyCollision(GameHandler.enemyList.get(i));
            //如果有碰撞则跳出循环
            if (result) break;
        }
    }

    //判断这颗子弹与某一敌机是否碰撞
    private boolean judgeEnemyCollision(Enemy enemy) {
        if (Calculate.isRectCollision(this.x, this.y, this.width, this.height,
                enemy.x, enemy.y, enemy.width, enemy.height)) {
            //如果碰撞的话就销毁自己和敌机
            this.isExplode = true;
            enemy.beHit();
            return true;
        }
        return false;
    }

    public void setTargetV(PVector targetV) {
        this.targetV = targetV;
    }

    public void setInitV(PVector initV) {
        this.initV = initV;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
