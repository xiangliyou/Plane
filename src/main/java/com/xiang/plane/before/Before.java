package com.xiang.plane.before;

import com.xiang.plane.MainApp;
import com.xiang.plane.MainFrame;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Create by Xiang Liyou on 2017/8/3
 *
 * @DESCRIPTION 飞机的开始界面
 */
public class Before {
    private int time;
    private int y;

    public Before() {
        time = 0;
        y = MainFrame.HEIGHT - 75;
    }

    public void draw(Graphics g) {
        g.setColor(Color.YELLOW);
        g.fillRect(0, 0, MainFrame.WIDTH, MainFrame.HEIGHT);
        g.setFont(new Font("楷体", Font.BOLD + Font.ITALIC, 30));
        g.setColor(Color.BLACK);
        g.drawString("Welcome", 30, 100);
        g.setFont(new Font("楷体", Font.BOLD + Font.ITALIC, 20));
        if (++time <= 10) {
            g.drawString("ENTER", 30, y);
        } else if (++time <= 20) {
            g.drawString("ENTER  >> ", 30, y);
        } else if (++time <= 30) {
            g.drawString("ENTER  >> >>", 30, y);
        } else if (++time <= 40) {
            g.drawString("ENTER  >> >> >> ", 30, y);
        } else if (++time <= 50) {
            g.drawString("ENTER  >> >> >> >>", 30, y);
        } else if (++time <= 60) {
            g.drawString("ENTER  >> >> >> >> >>", 30, y);
        } else if (++time <= 70) {
            g.drawString("ENTER  >> >> >> >> >> >>", 30, y);
            time = 0;
        }
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            MainApp.status = 1;
        }
    }

    public void keyReleased(KeyEvent e) {

    }

}
