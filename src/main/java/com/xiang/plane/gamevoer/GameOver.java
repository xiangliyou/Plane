package com.xiang.plane.gamevoer;

import com.xiang.plane.MainApp;;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Create by Xiang Liyou on 2017/8/3
 *
 * @DESCRIPTION
 */
public class GameOver {

    public void draw(Graphics g) {
        g.setColor(new Color(5,100,222));
        g.setFont(new Font("楷体", Font.BOLD, 20));
        g.drawString(MainApp.score+"", 10,50);
    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent e) {

    }
}
